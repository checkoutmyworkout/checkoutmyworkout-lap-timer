import {
  NativeModules,
  NativeEventEmitter,
} from 'react-native';
var Ticker = NativeModules.Ticker;
var TickerEmitter = new NativeEventEmitter(Ticker);

export const LapTimerCodes = {
  LAP_CHANGE: 'Lap Change',
  STOP: 'Stop',
  TICK: 'Tick'
};

export class LapTimer {

  isTimerActive = false;
  tickSubscription = null;

  startTimer(timerConfiguration, callback) {
    if (this.isTimerActive) {
      throw "only one timer can be run at a time";
    }

    this.validate(timerConfiguration, callback);

    this.isTimerActive = true;
    let currentTimerConfig = timerConfiguration;
    let accumlatedSeconds = 0;
    let lapAccumlatedSeconds = 0;
    let lapName = currentTimerConfig.laps[0];
    let lapSeconds = currentTimerConfig.laps.length == 1 ? currentTimerConfig.totalSeconds : currentTimerConfig.laps[1].startSecond - currentTimerConfig.laps[0].startSecond;
    let myObject = this;
    
    this.tickSubscription = TickerEmitter.addListener('TickMessage', (message) => {
      let lapTimerCode = LapTimerCodes.TICK;
      let lapProgress = 1.0;
      let totalProgress = 1.0;

      accumlatedSeconds++;
      lapAccumlatedSeconds++;
  
      if (accumlatedSeconds >= currentTimerConfig.totalSeconds) {
        lapTimerCode = LapTimerCodes.STOP;
        myObject.stopTimer();
      }
      else {
        for (let index = 0; index < currentTimerConfig.laps.length; index++) {
          if (accumlatedSeconds === currentTimerConfig.laps[index].startSecond) {
            lapTimerCode = LapTimerCodes.LAP_CHANGE;
            lapName = currentTimerConfig.laps[index].lapName;
            lapAccumlatedSeconds = 0;
            lapSeconds = index == currentTimerConfig.laps.length - 1 ? currentTimerConfig.totalSeconds : currentTimerConfig.laps[index+1].startSecond - currentTimerConfig.laps[index].startSecond;
          }
        }

        lapProgress = lapAccumlatedSeconds / lapSeconds;
        totalProgress = accumlatedSeconds / currentTimerConfig.totalSeconds;
      }
  
      callback({
        lapTimerCode: lapTimerCode, 
        lapName: lapName,
        accumlatedSeconds: accumlatedSeconds,
        lapProgress: lapProgress,
        totalProgress: totalProgress
      }, null);
    });

    Ticker.go();
  }

  stopTimer() {
    this.tickSubscription.remove();
    Ticker.stop();
    this.isTimerActive = false;
  }

  validate(timerConfiguration, callback) {
    if (!callback) {
      throw 'callback must be defined';
    }

    if (!timerConfiguration) {
      throw 'timerConfiguration must be defined';
    }

    if (!timerConfiguration.totalSeconds) {
      throw 'timerConfiguration.totalSeconds must be defined';
    }

    if (!timerConfiguration.laps) {
      throw 'timerConfiguration.laps must be defined';
    }

    if (!timerConfiguration.laps.length > 0) {
      throw 'timerConfiguration.laps array must contain at least one lap object';
    }

    let lastStartSecond = 0;
    for (let index = 0; index < timerConfiguration.laps.length; index++) {
      if (!timerConfiguration.laps[index].lapName) {
        throw `timerConfiguration.laps[${index}].lapName must be defined`;
      }

      if (!timerConfiguration.laps[index].startSecond) {
        throw `timerConfiguration.laps[${index}].startSecond must be defined`;
      }

      if (timerConfiguration.laps[index].startSecond <= lastStartSecond) {
        throw `timerConfiguration.laps[${index}].startSecond must be greater than the preceeding lap start second`;
      }

      lastStartSecond = timerConfiguration.laps[index].startSecond;
    }
  
  }

}
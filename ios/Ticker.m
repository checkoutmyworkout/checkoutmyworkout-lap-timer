//
//  LaptTimer.m
//  checkoutmyworkout
//
//  Created by Brian Thomas Webb in 2018.
//  Copyright © 2018 Brian Thomas Webb. All rights reserved.
//
#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>

#import <React/RCTBridgeModule.h>
#import <React/RCTEventEmitter.h>

@interface Ticker : RCTEventEmitter <RCTBridgeModule> {
  NSInteger tickAccumulated;
  NSTimer *tickTimer;
  BOOL isPaused;
}

@property(readwrite) NSInteger tickAccumulated;
@property(readwrite) BOOL isPaused;

-(void)tick:(NSTimer *)timer;

@end

@implementation Ticker
@synthesize tickAccumulated, isPaused;

RCT_EXPORT_MODULE();

- (id)init 
{
  self = [super init];
  if (self) {
    self.isPaused = NO;
    self.tickAccumulated = 0;
  }

  return self;
}

- (NSArray<NSString *> *)supportedEvents
{
  return @[@"TickerMessage"];
}

RCT_EXPORT_METHOD(go)
{
  self.tickAccumulated = 0;
  self.isPaused = NO;
  dispatch_async(dispatch_get_main_queue(), ^(void){
    tickTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tick:) userInfo:nil repeats:YES];
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
  });
  NSLog(@"Native lap timer started");
}

RCT_EXPORT_METHOD(stop)
{
  self.tickAccumulated = 0;
  self.isPaused = NO;
  dispatch_async(dispatch_get_main_queue(), ^(void){
    [tickTimer invalidate];
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
  });
  NSLog(@"Native lap timer stopped");
}

RCT_EXPORT_METHOD(pauseResume)
{
  self.isPaused = !self.isPaused;
  if (self.isPaused) {
    dispatch_async(dispatch_get_main_queue(), ^(void){
      [tickTimer invalidate];
      [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    });
    NSLog(@"Native lap timer paused");
  }
  else {
    dispatch_async(dispatch_get_main_queue(), ^(void){
      tickTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(tick:) userInfo:nil repeats:YES];
      [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    });
    NSLog(@"Native lap timer resumed");
  }

}

-(void)tick:(NSTimer *)timer
{
  tickAccumulated++;

  NSLog(@"Ticker.tick: tickAccumulated = %d", (int)tickAccumulated);
  [self sendEventWithName:@"TickeMessage"
    body:@{
      @"type": @"Tick",
      @"tickAccumulated": [NSNumber numberWithLong:self.tickAccumulated]
    }
  ];
}

@end

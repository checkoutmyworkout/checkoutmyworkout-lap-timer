import { LapTimer, LapTimerCodes } from '..';

// The Timer will call your code back at these lap intervals
// with the lap name indicated. In between, it will first
// a 'Tick' message every seccond.
const TOTAL_SECONDS = 8;
export const testingLapTimerConfig = {
  totalSeconds: TOTAL_SECONDS,
  laps: [
    { lapName: 'Warm Up', startSecond: 1},
    { lapName: 'Lap 1', startSecond: TOTAL_SECONDS-6},
    { lapName: 'Lap 2', startSecond: TOTAL_SECONDS-4},
    { lapName: 'Lap 3', startSecond: TOTAL_SECONDS-2},
    { lapName: 'Cool Down', startSecond: TOTAL_SECONDS-1},
  ]
};

var mockListener = null;
var mockTickAccumulated = 0;
var mockTimerId = null

jest.mock('NativeModules', () => {
  return {
    Ticker: {
      go: () => {
        mockTickAccumulated = 0;
        mockTimerId = setInterval(() => {
          mockTickAccumulated++;
    
          mockListener({
            type: 'Tick',
            tickAccumulated: mockTickAccumulated
          });
        }, 1);
      },
    
      stop: () => {
        mockTickAccumulated = 0;
        if (mockTimerId) {
          clearInterval(mockTimerId);
          mockTimerId = null;
        }
      },
    
      pauseResume: () => {
      }
    }
  };
});

jest.mock('NativeEventEmitter', () => {
  return function(nativeModule) { 
    let ticker = nativeModule;

    return {
      addListener: (eventType, listener, context) => {
        mockListener = listener;

        return {
          remove: () => {/*it's just a test, we can no-op this*/}
        }
      }
    };
  }
});

describe('lap timer test', () => {

  test('run the lap timer', done => {
    const lt = new LapTimer();
    let ta = 0;
    let currentLap = 0;

    function callback (message) {
      ta++;
      expect(message.accumlatedSeconds).toEqual(ta);

      if (message.accumlatedSeconds === testingLapTimerConfig.laps[currentLap].startSecond) {
        expect(message.lapTimerCode).toEqual(LapTimerCodes.LAP_CHANGE);

        if (currentLap < testingLapTimerConfig.laps.length) {
          expect(message.lapName).toEqual(testingLapTimerConfig.laps[currentLap].lapName);

          if (currentLap + 1 < testingLapTimerConfig.laps.length) {
            currentLap++;
          }
        }
      }
      else if (ta === testingLapTimerConfig.totalSeconds) {
        expect(message.lapTimerCode).toEqual(LapTimerCodes.STOP);
        done();
      }
      else {
        expect(message.lapTimerCode).toEqual(LapTimerCodes.TICK);
      }

      expect(message.totalProgress).toEqual(ta * 0.125);
      // console.log('ta=' + ta + ', message=' + JSON.stringify(message));
    }

    lt.startTimer(testingLapTimerConfig, callback);

  })

}); 

describe('lap timer configuratio validation tests', () => {

  test('config must be defined', () => {
    expect(() => {
      new LapTimer().startTimer(null, () => {});
    }).toThrowError('timerConfiguration must be defined');
  });

  test('callback must be defined', () => {
    expect(() => {
      new LapTimer().startTimer(testingLapTimerConfig, null);
    }).toThrowError('callback must be defined');
  });

  test('totalSeconds must be defined', () => {
    expect(() => {
      new LapTimer().startTimer({laps:[{lapName:'test1', startSecond: 1}]}, () => {});
    }).toThrowError('timerConfiguration.totalSeconds must be defined');
  });

  test('timerConfiguration.laps must be defined', () => {
    expect(() => {
      new LapTimer().startTimer({totalSeconds:120}, () => {});
    }).toThrowError('timerConfiguration.laps must be defined');
  });

  test('timerConfiguration.laps array must contain at least one lap object', () => {
    expect(() => {
      new LapTimer().startTimer({totalSeconds:120, laps: []}, () => {});
    }).toThrowError('timerConfiguration.laps array must contain at least one lap object');
  });

  test('timerConfiguration.laps[0].startSecond must be defined', () => {
    expect(() => {
      new LapTimer().startTimer({totalSeconds:120, laps: [{lapName:'test1', startSecond: 0}]}, () => {});
    }).toThrowError('timerConfiguration.laps[0].startSecond must be defined');
  });

  test('timerConfiguration.laps[2].startSecond must be greater than the preceeding lap start second', () => {
    expect(() => {
      new LapTimer().startTimer(
        {
          totalSeconds:120, 
          laps: [
            {lapName:'test1', startSecond: 1},
            {lapName:'test1', startSecond: 30},
            {lapName:'test1', startSecond: 20}
          ]
        }, 
        () => {});
    }).toThrowError('timerConfiguration.laps[2].startSecond must be greater than the preceeding lap start second');
  });

}); 